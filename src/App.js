import React, { useEffect, useState, useRef } from 'react';
import NProgress from 'nprogress'


import './App.scss';
import test from './images/testHD.png';

import Sidebar from './components/Sidebar'
import Header from './components/Header'

import { Image as ImageJS } from 'image-js' 

function App() {

  // REF

  const cvsRef = useRef()
  const [showman, setShowman] = useState(false)
  const [aimsState, setAimsState] = useState(0)

  // CANVASES
  const [canvas, setCanvas] = useState();
  const [ctx, setCtx] =  useState();

  const [convertedCanvas, setConvertedCanvas] = useState()
  const [convertedCtx, setConvertedCtx] = useState()

  const [bboxCanvas, setBboxCanvas] = useState()
  const [bboxCtx, setBboxCtx] = useState()
  const [open, setOpen] = useState(true)
  const [thresh, setThresh] = useState(0.9)
  const [convLevel, setConvLevel] = useState(0)
  const [observations, setObservations] = useState(null)
  const [activeImg, setActiveimg] = useState(null)
  const[loading, setLoading] = useState(false)

 
  useEffect(()=>{
    console.log("EFFECT1")
    setCanvas(document.getElementById("cv"))
    if(canvas){
      setCtx(canvas.getContext("2d"))
    }
  },[canvas,ctx]);

  useEffect( () => {
    console.log("EFFECT2")
    setBboxCanvas(document.getElementById("cv2"))
    if (bboxCanvas) {
      setBboxCtx(bboxCanvas.getContext("2d"))
    }
  }, [bboxCanvas, bboxCtx])

  useEffect( () => {
    console.log("EFFECT3")
    setConvertedCanvas(document.getElementById("cv3"))
    if (convertedCanvas) {
      setConvertedCtx(convertedCanvas.getContext("2d"))
    }
  }, [convertedCanvas, convertedCtx])


  useEffect( () => {
    refreshBoxes()
  }, [observations, thresh])

  




  const loadImage = async(im, cb, isGray) => {



    console.log("LOADIMAGE")
    
    let img = new Image()
    img.src = im

    if (!isGray) {
      setActiveimg(im)
      setLoading(true)
    }
    
    
    setObservations([])
    
    img.onload= ()=>{
      
      if (isGray) {
        convertedCanvas.height = img.naturalHeight
        convertedCanvas.width = img.naturalWidth
        convertedCtx.clearRect(0, 0, convertedCanvas.width, convertedCanvas.height)
        convertedCtx.drawImage(img,0,0,img.naturalWidth,img.naturalHeight);
      } else {
        canvas.height = img.naturalHeight
        canvas.width = img.naturalWidth
        bboxCanvas.height = img.naturalHeight
        bboxCanvas.width = img.naturalWidth
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        ctx.drawImage(img,0,0,img.naturalWidth,img.naturalHeight);
        setLoading(false)
      }
      cb()
    }
  }

  const refreshBoxes = () => {
      if(bboxCtx) {
        bboxCtx.clearRect(0, 0, bboxCanvas.width, bboxCanvas.height);
      }
      observations && observations.map((obs, key)=> {
        drawBbox(obs, key)
      })
  }

  const drawBbox = (observation, key) => {
    console.log("DRAWBBOX")
    const { xmin, xmax, ymin, ymax, confidence } = observation

    if (key == 0) {
      console.log(observation)
    }

    
    if(confidence>=thresh || key == 0){
      bboxCtx.beginPath();
      bboxCtx.lineWidth = "3";
      bboxCtx.strokeStyle = "red";
      bboxCtx.rect(
        xmin,
        ymin,
        Math.abs(xmax-xmin),
        Math.abs(ymax-ymin)
      );
      bboxCtx.stroke();
    }
  }

  const testAPI = async(cb) =>{
    setAimsState(1)
    setConvLevel(0)
    fetch(activeImg,{
      method:'get'
    }).then((response)=> {
      return response.blob()
    }).then((blob)=> {

      let payload = new FormData()
      payload.append('image', blob);

      NProgress.start();
      fetch('https://aims.alixir.ai/magic',{
        method:'post',
        body: payload,
      })
      .then( res =>{
        console.log(res)
        return res.json()
      })
      .then(readings => {
        setAimsState(2)
        showmanship().then( () => {
          setShowman(false)
          setConvLevel(0)
          setObservations(readings)
          NProgress.done()
          setAimsState(0)
          cb()
        })
        
        
      })    //print data to console
      .catch(err => {
        alert(`Request Failed : ${err}`)
        NProgress.done()
      }); // Catch errors    
    })
  }



  const showmanship = () => {
    return new Promise( async (resolve) => {
      setShowman(true)
      for (var c = 0.5; c < 1.0; c+=0.01) {
        await delay(100)
        setConvLevel(c)
      }
      resolve("OK")
    })
  }

  const delay = (ms) => {
    return new Promise( (resolve) => {
      setTimeout( () => {
        resolve()
      }, ms)
    })
  }

  const toggleOpen = () => {
    setOpen(!open)
  }

  const updateThreshold = async(t) => {
    setThresh(t)
  }

  const updateConversion = (a) => {
    setConvLevel(a)
  }

  return (
    <div className="App">
      <section className="app-section">
        <Header toggleOpen={toggleOpen}/>
        
        <div className={!open ? " content expanded" : "content"} >

          

          <div className="cvs-wrap">
          { loading && <div className="loader">LOADING...</div>}
            <canvas ref={cvsRef} className={!open && "expanded"} id="cv" style={{opacity: convLevel}} />
            <canvas id="cv2" style={{position: 'absolute', zIndex: 999, top: 0, left: 0}} />
            <canvas id="cv3" style={{position: 'absolute', top: 0, left: 0, opacity: 1-convLevel}} className={showman && 'cvs'} />
          </div>          
          
          <Sidebar aimsState={aimsState} ready={ctx && convertedCtx && bboxCtx} convLevel={convLevel} updateConversion={updateConversion} updateThresh={updateThreshold} thresh={thresh} openState={open} loadImage={loadImage} testAPI={testAPI} />
        </div>
      </section>
    </div>
  );
}

export default App;
