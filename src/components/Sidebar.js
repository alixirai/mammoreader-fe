import { render } from '@testing-library/react'
import React, { useState, useEffect } from 'react'


import m1 from '../images/1E0725.png';
import m2 from '../images/4E5F19.png';
import m3 from '../images/4F5392.png';
import m4 from '../images/7AD394.png';
import m5 from '../images/33E5AA.png';
import m6 from '../images/413FD5.png';
import m7 from '../images/B4DA2A.png';
import m8 from '../images/B5E43F.png';
import m9 from '../images/C3AFAE.png';
import m10 from '../images/DA428A.png';



import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Image as ImageJS } from 'image-js' 

const images = [m1, m2, m3, m4, m5, m6, m7, m8, m9, m10]


const Sidebar = (props) => {

  const [open, setOpen] = useState(true)
  const [currentImage, setCurrentImage] = useState(0)
  const [detState, setDetState] = useState(0)
  const [loadingGrayscale, setLoadingGrayscale] = useState(false)
  const [loadingConverted, setLoadingConverted] = useState(false)
  const [detecting, setDetecting] = useState(false)

  useEffect( () => {
    if (props.ready) {
      loadImage()
    }
  }, [props.ready])
   

  const toggleOpen = () => {
    setOpen(!open)
  }

  const loadImage = () => {
    if (currentImage == images.length - 1) {
      console.log("END OF ARR")
      setCurrentImage(0)
    } else {
      setCurrentImage(currentImage + 1)
    }
    setDetState(0)
    const im = images[currentImage == images.length - 1 ? 0 : currentImage + 1]
    setLoadingGrayscale(true)

    props.loadImage(im, () => {
      setDetState(1)
      setLoadingGrayscale(false)
    }, false)

    ImageJS.load(im).then(function (image) {
      var im2 = image.getChannel(2)
      props.loadImage(im2.toDataURL('image/png'), () => {
        setLoadingGrayscale(false)
        setDetState(1)
      }, true)
    });
  }

  const gray = () => {
    const im = images[currentImage]
    ImageJS.load(im).then(function (image) {
      var im2 = image.getChannel(2)
      props.loadImage(im2.toDataURL('image/png'), () => {
      })
      
      
    });
  }

  const getConverted = () => { 
    setLoadingConverted(true)
    const im = images[currentImage]
    setTimeout( () => {
      props.loadImage(im, () => {
        setDetState(2)
        setLoadingConverted(false)
      })
    }, 5000)

  }

  const detect = () => {
    setDetState(3)
    setDetecting(true)
    props.testAPI( () => {
      setDetecting(false)
    })
  }
    
    return (
        <div className={props.openState ? "sidebar open" : "sidebar"}>
            <button className={props.openState ? "test-btn" : 'condensed test-btn'} onClick={ () => loadImage()}>
              <CloudUploadIcon />
          { props.openState && <p>{loadingGrayscale ? 'Loading Mammogram' : 'Load New Mammogram'}</p> }
        </button>
        <button 
          className={props.openState ? "test-btn" : 'condensed test-btn'} onClick={ () => detect()}>
          <img src="/logo.png" />
          { props.openState && <p>
            {props.aimsState == 0 && 'Run AIMS'}
            {props.aimsState == 1 && 'Uploading DICOM'}
            {props.aimsState == 2 && 'Detecting...'}
            </p> }
        </button>

        <div className="slider">
          <p>AIMS Preprocessing {props.convLevel}</p>
          <input 
            onChange={ (e) => props.updateConversion(e.target.value)}
            type="range" value={parseFloat(props.convLevel).toFixed(2)} min={0.0} max={1.0} step={0.01}/>
        </div>

        <div className="slider">
          <p>ML Threshold {props.thresh}</p>
          <input 
            onChange={ (e) => props.updateThresh(e.target.value)}
            type="range" defaultValue={props.thresh} min={0.0} max={1.0} step={0.01}/>
        </div>
        
        </div>
        
    )

}

export default Sidebar