import MenuIcon from '@material-ui/icons/Menu';

const Header = (props) => {
    return (
        <header>
            <img src="/logo.png" />
            <button className="menu-btn" onClick={ props.toggleOpen}><MenuIcon /></button>
        </header>
    )
}

export default Header